const AppDB = require('./appDB');
const express = require('express')
const cors = require('cors')
const app = express()
const employees = require('./data/employees.json');
let bodyParser = require('body-parser');

var corsOptions = {
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200,
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
  preflightContinue: true
}
var db = new AppDB('./database.employee');
db.run(`CREATE TABLE IF NOT EXISTS employees (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT,
  code TEXT,
  profession TEXT,
  color TEXT,
  city TEXT,
  branch TEXT,
  assigned BOOLEAN)`);

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());

/*CRUD GET*/
app.get('/api/employees', cors(corsOptions), (req, res, next) => {
  db.all(`SELECT * FROM employees`).then((result) => {
    res.setHeader('Content-Type', 'application/json');
    res.status(200);
    res.json(result);
  })
});
app.options('/api/employee', cors());

/*CRUD POST*/
app.post('/api/employee', cors(), (req, res, next) => {
  const response = req.body;
  db.run(`INSERT INTO employees (name, code, profession, color, city, branch, assigned) 
  VALUES (?,?,?,?,?,?,?)`,[response.name, response.code, response.profession, response.color, response.city, response.branch, response.assigned]).
  then(response => {
    db.all(`SELECT * FROM employees WHERE id = (SELECT MAX(ID) FROM employees)`).then(result => {
      console.log(result);
      res.json({text: 'success', item: result});
    })
  })
});
app.options('/api/employee/:id', cors());

/*CRUD PUT*/
app.put('/api/employee/:id', cors(), (req, res) => {
  const response = req.body;
  db.run(`UPDATE employees SET name=?, code=?, profession=?, color=?, city=?, branch=?, assigned=? WHERE id = ?`, [response.name, response.code, response.profession, response.color, response.city, response.branch, response.assigned, req.params.id]).
  then(response => {
    res.json({text: 'success', id: req.params.id});
  }).catch(err => {
    res.json({text: 'error', id: req.params.id})
  })
});

/*CRUD DELETE*/
app.delete('/api/employee/:id', cors(), (req, res) => {
    db.run(`DELETE FROM employees WHERE id = ?`, [req.params.id]).
    then(response => {
      res.json({text: 'success', id: req.params.id});
    }).catch(err => {
      res.json({text: 'fail'});
    })
});
app.listen(8080, () => console.log('Job Dispatch API running on port 8080!'))