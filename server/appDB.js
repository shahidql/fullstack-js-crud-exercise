const sqlite3 = require('sqlite3');

module.exports = AppDB = function(path) {
    var db;
    var dbFilePath = path;
    var inialize = function() {
        db = new sqlite3.Database(dbFilePath, (err) => {
            if (err) {
              console.log('Could not connect to database', err)
            } else {
              console.log('Connected to database')
            }
        })
    }
    inialize();
    return {
        run: function(sql, params) {
            return new Promise((resolve, reject) => {
                db.run(sql, params, (err) => {
                    if (err) {
                      console.log('Error running sql ' + err)
                      reject(err)
                    } else {
                      resolve(true)
                    }
                  })  
            })
        },
        all: function(sql, params = []) {
            return new Promise((resolve, reject) => {
                db.all(sql,params, (err, results) => {
                    if(err) {
                        reject(err);
                    } else {
                        resolve(results);
                    }
                });
            })
        },
        execute: function(sql, params = []) {
            return new Promise((resolve, reject) => {
                db.get(sql, params, (err, result) => {  
                    if (err) {
                        reject(err)
                    } else {
                        resolve(result)
                    }
                  })
            })
        }
    }
}