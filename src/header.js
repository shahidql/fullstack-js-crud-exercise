import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './header.css';

export default class Header extends Component {
    static propTypes  = {
        name: PropTypes.string.isRequired
    }
    render() {
        return (
            <header>
                <div>{this.props.name}</div>
                <div className="overlay"></div>
            </header>
            )
    }
}