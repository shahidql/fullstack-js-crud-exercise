import React, {Component} from 'react';
//import PropType from 'prop-types';
import './list.css';

const Edit = (props) => { return <input id={props.id} name="edit" type="radio" value={props.value} checked={props.value === true ? true : false} onChange={props.editHandler} /> }
const Input = (props) => { return <input type="text" placeholder={props.placeholder} name={props.name} value={props.value} onChange={props.onTrigger}/> }
const Radio = (props) => (<div><span>Assigned:<input type="radio" name={props.name} value={true} defaultChecked={props.value === "true"} onChange={props.onTrigger}/></span><span>Unassinged:<input type="radio" name={props.name} value={false} defaultChecked={props.value === "false"} onChange={props.onTrigger}/></span></div>)

export default class List extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            employees: [],
            index: null,
            name: '',
            profession: '',
            color: '',
            city: '',
            branch: '',
            assigned: '',
            disabled: 'disabled' 
        }
        this.save = this.save.bind(this);
        this.manipulate = this.manipulate.bind(this);
    }

    componentDidMount() {
        let list = [];
        fetch('http://localhost:8080/api/employees')
        .then(response => response.json())
        .then(employees => {
            employees.map((key,i) => {
                list[i] = false;
                this.setState({ employees , list});
                return;
            })
        })
    }

    changeHandler(e) {
        const name = e.target.name.toLowerCase();
        const value = e.target.value;
        this.setState({[name]: value});
    }

    cancel() {
        let list = this.state.list;
        list[this.state.index] = false;
        this.setState({disabled:'disabled',list})
    }

    save(type) { 
        this.manipulate(null, type);
    }

    manipulate(e, type) { 
        if(type === undefined) return 
        let endpoint; 
        let list = this.state.list;
        if(type === 'post') {
            endpoint = 'http://localhost:8080/api/employee'; 
        } else if (type === 'put' || type === 'delete') {
            endpoint = `http://localhost:8080/api/employee/${this.state.activeId}`;
        }
        fetch(endpoint, {
            method: type,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state)
        }).then(response => {
            if(response.status === 200 && type === 'post') {
                response.json().then(record => {
                    let newRecord = [...this.state.employees, ...record.item];
                    let {name, code, profession, color, city, branch, assigned} = {name:'',code:'',color:'',city:'',branch:'',assigned:''};
                    this.setState({employees: newRecord, hasForm: false, name, code, profession, color, city, branch, assigned});
                })
            } else if(response.status === 200 && type === 'delete') {
                let emp = Object.values(Object.assign({},this.state.employees));
                response.json().then(res => {
                    const delEmployee = emp.filter(employee => {
                        return employee.id === res.id
                    });
                    const deletedIndex = emp.indexOf(delEmployee[0]);
                    list[deletedIndex] = false;
                    console.log('id:',res.id,'====',deletedIndex,'===',delEmployee);
                    emp.splice(deletedIndex,1);
                    this.setState({employees: emp, list, disabled: 'disabled'});
                    console.log(emp,'===state:',this.state.employees);
                })
            } else if(response.status === 200 && type === 'put') {
                response.json().then(res => {
                    list[this.state.index] = false;
                    let temp = [...this.state.employees];
                    const {id,name,code,profession,color,city,branch,assigned} = this.state;
                    temp.splice(this.state.index,1,{id,name,code,profession,color,city,branch,assigned});
                    temp[this.state.index].id = this.state.activeId;
                    this.setState({list,employees:temp})
                })
            }
        })
    }

    new() {
        this.setState({hasForm: true});
        const {name,code,profession,color,city,branch,assigned} = {name:'',code:'',profession:'',color:'',city:'',branch:'',assigned:''};
        this.setState({name,code,profession,color,city,branch,assigned});
    }

    delete() {
        this.manipulate(null,'delete');
    }

    closeForm() {
        this.setState({hasForm: false})
    }

    form() {
        return (
            this.state.hasForm ? <div className="form">
                <div><label>Name:</label><Input name="name" placeholder={'Name'} onTrigger={e => this.changeHandler(e)} value={this.state.name} /></div>
                <div><label>Code:</label><Input name="code" placeholder={'Code'} onTrigger={e => this.changeHandler(e)} value={this.state.code} /></div>
                <div><label>Profession:</label><Input name="profession" placeholder={'Profession'} onTrigger={e => this.changeHandler(e)} value={this.state.profession} /></div>
                <div><label>Color:</label><Input name="color" placeholder={'Color'} onTrigger={e => this.changeHandler(e)} value={this.state.color} /></div>
                <div><label>City:</label><Input name="city" placeholder={'City'} onTrigger={e => this.changeHandler(e)} value={this.state.city} /></div>
                <div><label>Branch:</label><Input name="branch" placeholder={'Branch'} onTrigger={e => this.changeHandler(e)} value={this.state.branch} /></div>
                <div><label>Assigned:</label><Input name="assigned" placeholder={'Assigned'} onTrigger={e => this.changeHandler(e)} value={this.state.assigned} /></div>
                    <div className="buttons-group">
                        <button onClick={this.closeForm.bind(this)}>Close</button>
                        <button onClick={e => this.save('post')}>Save</button>
                    </div>
            </div>:null
        )
    }
    
    customProps(x) {
        return {value: x,changeHandler: this.changeHandler}
    }

    editProps(x) {
        return {edit: x,editHandler: this.editHandler, index: this.state.index}
    }

    editHandler(e,id,indx) {
        if(this.state.disabled !== 'disabled') return;
        let list = this.state.list;
        if(e.target.checked) {
            const selEmployee = this.state.employees.filter(employee => {
                return employee.id === id
            });
            list[indx] = true;
            const {name,code,profession,color,city,branch,assigned} = selEmployee[selEmployee.length - 1];
            this.setState({name, code, profession, color, city, branch, assigned, disabled: '', list, index: indx, activeId: id});
            this.closeForm();
        } else {
            list[indx] = false;
            this.setState({disabled:'disabled', list})
        }
        console.log(this.state)
    }

    header(employees) {
        return  employees.map((key,i) => i === 0  ? <ul key={i+'ind'} className="headr">{
            Object.keys(key).map(title => title === 'id' ? <li key={title} className="edit">Edit</li> : <li key={title}>{title}</li>)
        }</ul> : null)
    }

    control() {
        let disabled = this.state.disabled;
        return (
            <div className="buttons-group">
            {disabled === 'disabled'? <button disabled>Cancel</button> : <button onClick={this.cancel.bind(this)}>Cancel</button>}
            {disabled === 'disabled'? <button disabled>Update</button> : <button onClick={e => this.save('put')}>Save</button> }
            {disabled === 'disabled'? <button disabled>Delete</button> : <button onClick={() => { window.confirm('Are you sure to delete items in selected row?')? this.delete() : null }}>Delete</button> }
            {disabled === 'disabled'? <button onClick={this.new.bind(this)}>Create</button> : <button disabled>Create</button>}
            </div>
        )
    }

    list(employee,index) {
        return (
            <ul key={index}>
                <li className="edit"><label htmlFor={'r-'+employee.id}><Edit id={'r-'+employee.id} value={this.state.list[index]} editHandler={e => this.editHandler(e,employee.id,index)} /></label></li>
                <li>{this.state.list[index]? <Input name="name" placeholder={'Name'} onTrigger={e => this.changeHandler(e)} value={this.state.name} /> : employee.name}</li>
                <li>{this.state.list[index]? <Input name="code" placeholder={'Code'} onTrigger={e => this.changeHandler(e)} value={this.state.code} /> : employee.code}</li>
                <li>{this.state.list[index]? <Input name="profession" placeholder={'Profession'} onTrigger={e => this.changeHandler(e)} value={this.state.profession} /> : employee.profession}</li>
                <li>{this.state.list[index]? <Input name="color" placeholder={'Color'} onTrigger={e => this.changeHandler(e)} value={this.state.color} /> : employee.color}</li>
                <li>{this.state.list[index]? <Input name="city" placeholder={'City'} onTrigger={e => this.changeHandler(e)} value={this.state.city} /> : employee.city}</li>
                <li>{this.state.list[index]? <Input name="branch" placeholder={'Branch'} onTrigger={e => this.changeHandler(e)} value={this.state.branch} /> : employee.branch}</li>
                <li>{this.state.list[index]? <Radio name="assigned" placeholder={'Assigned'} onTrigger={e => this.changeHandler(e)} value={this.state.assigned} /> : employee.assigned === "true" ? 'Assigned':'UnAssigned'}</li>
            </ul>
        )
    }

    render() {
        const { employees } = this.state;
        return (
            <div className="content">
                <div className="section">
                    { employees && this.header(employees)}
                    { employees && employees.map((employee, i) => this.list(employee,i))}
                    { this.control() }
                    { this.form()}
                </div>
            </div>
        )
    }
}