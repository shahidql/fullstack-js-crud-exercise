import React from 'react';
import Header from './header';
import Footer from './footer';
import List from './list';
import './App.css';

class App extends React.Component {
  
  render() {
    let list = [
      {title: 'Mission', link: ['OVERVIEW','SOLUTIONS','SECURITY','SUPPORT','GOVERNMENT','CONTRACT VEHICLES']}, 
      {title: 'Privacy Policy', link: ['SECURITY','SUPPORT','GOVERNMENT','CONTRACT VEHICLES']}, 
      {title: 'Terms Of Policy', link: ['ABOUT US','CONTACT']}, 
      {title: 'Map', link: ['Domain A']}];

    return (
      <div className="App">
        <Header name={"Header"}/>
        <List />
        <Footer items={list}/>
      </div>
    );
  }
}

export default App;
