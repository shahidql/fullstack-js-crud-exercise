import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './footer.css';

export default class Footer extends Component {

    static propTypes = {
        items: PropTypes.array.isRequired
    }

    render() {
        return (
            <footer>
                <div>
                    {this.props.items.map((item,i) => (
                        <ul key={item.title}><li className="title">
                            <span>{item.title}</span>
                            {item.link? <ul>{item.link.map(i => (<li key={i}><a  href={'/'+i}>{i}</a></li>))}</ul>:null}
                            </li>
                        </ul>)
                    )}
                </div>
            </footer>
        )
    }
}